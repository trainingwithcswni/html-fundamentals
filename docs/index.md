# Bienvenido
![HTML](https://image.shutterstock.com/image-vector/isometric-3d-teamwork-building-do-600w-1341632465.jpg)

Este proyecto consiste en brindar fundamentos generales sobre conceptos de HTML.

## Link del manual
[Enlace en GitLab Pages - HTML Fundamental](https://trainingwithcswni.gitlab.io/html-fundamentals/)

## Contenido
* ¿Qué es el HTML?
* ¿Aspecto del HTML?
* ¿La historia del HTML?

