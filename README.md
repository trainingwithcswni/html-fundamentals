# html-fundamentals

HTML de introducción para tus primeras páginas web

## Getting started

Curso de html5 para principiantes.

## Autor
Equipo de Training CSWNI

## Equipo de trabajo

| Usuario        | Nombres                    |
|----------------|----------------------------|
| @cswni         | Carlos Andres Perez Ubeda  |
| @Kratos007-dev | Luis Angel Benavidez Gamez |

## Licencia

Copyright 2022 Training CSWNI Team

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.